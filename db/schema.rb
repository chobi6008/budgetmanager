# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150217083354) do

  create_table "categories", force: true do |t|
    t.string   "name"
    t.integer  "fiscal_year_id"
    t.integer  "category_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "category_budget_details", force: true do |t|
    t.integer  "category_budget_id"
    t.date     "budget_date"
    t.string   "section"
    t.string   "pay_for"
    t.string   "type"
    t.string   "code"
    t.decimal  "unit_price",         precision: 14, scale: 4
    t.decimal  "units",              precision: 14, scale: 4
    t.string   "currency"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "category_budgets", force: true do |t|
    t.integer  "category_version_id"
    t.integer  "version"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "category_types", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "category_versions", force: true do |t|
    t.integer  "category_id"
    t.integer  "version"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "category_wbs_versions", force: true do |t|
    t.integer  "category_id"
    t.integer  "wbs_version_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fiscal_years", force: true do |t|
    t.integer  "fiscal_year"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "forecast_details", force: true do |t|
    t.integer  "forecast_id"
    t.date     "forecast_date"
    t.string   "section"
    t.string   "pay_for"
    t.string   "type"
    t.string   "code"
    t.decimal  "unit_price",    precision: 14, scale: 4
    t.decimal  "units",         precision: 14, scale: 4
    t.string   "currency"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "forecasts", force: true do |t|
    t.integer  "wbs_item_id"
    t.integer  "version"
    t.date     "input_date"
    t.boolean  "islocked"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "wbs_budget_details", force: true do |t|
    t.integer  "wbs_budget_id"
    t.date     "budget_date"
    t.string   "section"
    t.string   "pay_for"
    t.string   "type"
    t.string   "code"
    t.decimal  "unit_price",    precision: 14, scale: 4
    t.decimal  "units",         precision: 14, scale: 4
    t.string   "currency"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "wbs_budgets", force: true do |t|
    t.integer  "wbs_version_id"
    t.integer  "version"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "wbs_items", force: true do |t|
    t.string   "name"
    t.string   "number"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "wbs_versions", force: true do |t|
    t.integer  "wbs_item_id"
    t.integer  "version"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
