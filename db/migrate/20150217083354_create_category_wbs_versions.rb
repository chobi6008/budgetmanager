class CreateCategoryWbsVersions < ActiveRecord::Migration
  def change
    create_table :category_wbs_versions do |t|
      t.integer :category_id
      t.integer :wbs_version_id

      t.timestamps
    end
  end
end
