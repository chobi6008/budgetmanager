class CreateForecasts < ActiveRecord::Migration
  def change
    create_table :forecasts do |t|
      t.integer :wbs_item_id
      t.integer :version
      t.date :input_date
      t.boolean :islocked

      t.timestamps
    end
  end
end
