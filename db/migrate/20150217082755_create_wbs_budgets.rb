class CreateWbsBudgets < ActiveRecord::Migration
  def change
    create_table :wbs_budgets do |t|
      t.integer :wbs_version_id
      t.integer :version

      t.timestamps
    end
  end
end
