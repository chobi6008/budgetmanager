class CreateCategoryVersions < ActiveRecord::Migration
  def change
    create_table :category_versions do |t|
      t.integer :category_id
      t.integer :version

      t.timestamps
    end
  end
end
