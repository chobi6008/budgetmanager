class CreateWbsItems < ActiveRecord::Migration
  def change
    create_table :wbs_items do |t|
      t.string :name
      t.string :number

      t.timestamps
    end
  end
end
