class CreateCategoryBudgets < ActiveRecord::Migration
  def change
    create_table :category_budgets do |t|
      t.integer :category_version_id
      t.integer :version

      t.timestamps
    end
  end
end
