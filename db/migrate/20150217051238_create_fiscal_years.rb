class CreateFiscalYears < ActiveRecord::Migration
  def change
    create_table :fiscal_years do |t|
      t.integer :fiscal_year

      t.timestamps
    end
  end
end
