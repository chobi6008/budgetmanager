class CreateWbsVersions < ActiveRecord::Migration
  def change
    create_table :wbs_versions do |t|
      t.integer :wbs_item_id
      t.integer :version

      t.timestamps
    end
  end
end
