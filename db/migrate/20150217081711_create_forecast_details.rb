class CreateForecastDetails < ActiveRecord::Migration
  def change
    create_table :forecast_details do |t|
      t.integer :forecast_id
      t.date :forecast_date
      t.string :section
      t.string :pay_for
      t.string :type
      t.string :code
      t.decimal :unit_price	, precision: 14, scale: 4, null: true
      t.decimal :units		, precision: 14, scale: 4, null: true
      t.string :currency

      t.timestamps
    end
  end
end
