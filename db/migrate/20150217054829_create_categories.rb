class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.integer :fiscal_year_id
      t.integer :category_type_id

      t.timestamps
    end
  end
end
