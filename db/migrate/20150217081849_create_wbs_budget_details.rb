class CreateWbsBudgetDetails < ActiveRecord::Migration
  def change
    create_table :wbs_budget_details do |t|
      t.integer :wbs_budget_id
      t.date :budget_date
      t.string :section
      t.string :pay_for
      t.string :type
      t.string :code
      t.decimal :unit_price	, precision: 14, scale: 4, null: true
      t.decimal :units		, precision: 14, scale: 4, null: true
      t.string :currency

      t.timestamps
    end
  end
end
