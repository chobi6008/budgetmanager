require 'test_helper'

class FiscalYearControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get show" do
    get :show
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get edit" do
    get :edit
    assert_response :success
  end

  test "should get create" do
    get :create
    assert_response :success
  end

  test "should get update" do
    get :update
    assert_response :success
  end

  test "should get newcat" do
    get :newcat
    assert_response :success
  end

  test "should get addcat" do
    get :addcat
    assert_response :success
  end

  test "should get deletecat" do
    get :deletecat
    assert_response :success
  end

end
