class FiscalYearController < ApplicationController
  def index
	  @fiscal_years = FiscalYear.all
  end

  def show
  end

  def new
	  @fiscal_year = FiscalYear.new
  end

  def edit
  end

  def create
  end

  def update
  end

  def newcat
  end

  def addcat
  end

  def deletecat
  end
end
