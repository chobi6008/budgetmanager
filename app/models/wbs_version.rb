class WbsVersion < ActiveRecord::Base
	belongs_to :wbs_item
	has_and_belongs_to_many :categories
	has_many :wbs_budgets
end
