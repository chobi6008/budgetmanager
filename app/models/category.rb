class Category < ActiveRecord::Base
	belongs_to :fiscal_year
	belongs_to :category_type
	has_and_belongs_to_many :wbs_versions
	has_many :category_versions
end
