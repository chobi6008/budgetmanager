class CategoryVersion < ActiveRecord::Base
	belongs_to :category
	has_many :category_versions
end
