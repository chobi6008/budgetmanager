class CategoryBudget < ActiveRecord::Base
	belongs_to :category_version
	has_many :category_version_details
end
