class Forecast < ActiveRecord::Base
	belongs_to :wbs_item
	has_many :forecast_details
end
