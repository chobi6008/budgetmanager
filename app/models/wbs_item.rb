class WbsItem < ActiveRecord::Base
	has_many :wbs_versions
	has_many :forecasts
end
