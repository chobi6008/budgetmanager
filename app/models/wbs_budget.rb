class WbsBudget < ActiveRecord::Base
	belongs_to :wbs_version
	has_many :wbs_budget_details
end
